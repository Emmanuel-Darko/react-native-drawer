import { StatusBar } from 'expo-status-bar';
import React, { useRef, useState } from 'react';
import { Animated, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import profile from './assets/profile.png';
import photo from './assets/photo.jpg'
import home from './assets/home.png';
import search from './assets/search.png';
import notifications from './assets/bell.png';
import settings from './assets/settings.png';
import logout from './assets/logout.png';
// Menu
import menu from './assets/menu.png';
import close from './assets/close.png';
import { transform } from 'lodash';

// import TabBottom from './TabBottom';

export default function App() {
  const [currentTab, setCurrentTab] = useState("Home");
  // To get the current status of menu
  const [showMenu, setShowMenu] = useState(false);

  // Animated properties
  const offsetValue = useRef(new Animated.Value(0)).current;
  // Scale initially must be 1
  const scaleValue = useRef(new Animated.Value(1)).current;
  const closeButtonOffset = useRef(new Animated.Value(0)).current;

  return (
    <SafeAreaView style={styles.container}>

      <View style={{justifyContent: 'flex-start', padding: 15}}>
        <TouchableOpacity>
          <Image source={profile} style={{
            width: 60,
            height: 60,
            borderRadius: 10,
            marginTop: 8
          }}
          ></Image>

        </TouchableOpacity>

        <Text style={{
          fontSize: 20,
          fontWeight: 'bold',
          color: 'white',
          marginTop: 20
        }}>  Kode </Text>

        <TouchableOpacity>
          <Text style={{
            marginTop: 6,
            color: 'white',
          }}> View Profile </Text>
        </TouchableOpacity>

        <View style={{flexGrow: 1, marginTop: 50}}>
          {/* Tab Bar Bottoms */}

          { TabButton( currentTab, setCurrentTab, "Home", home, scaleValue, offsetValue, closeButtonOffset, showMenu, setShowMenu ) }
          { TabButton( currentTab, setCurrentTab, "Search", search, scaleValue, offsetValue, closeButtonOffset, showMenu, setShowMenu ) }
          { TabButton( currentTab, setCurrentTab, "Notifications", notifications, scaleValue, offsetValue, closeButtonOffset, showMenu, setShowMenu ) }
          { TabButton( currentTab, setCurrentTab, "Settings", settings, scaleValue, offsetValue, closeButtonOffset, showMenu, setShowMenu ) }
        </View>

        <View>
          { TabButton( currentTab, setCurrentTab, "Logout", logout ) }
        </View>

      </View>
    
      {/* Overlay View */}
      <Animated.View style={{
        flexGrow: 1,
        backgroundColor: 'white',
        position: 'absolute',
        top: 0, bottom: 0, left: 0, right: 0,
        paddingHorizontal: 15,
        paddingVertical: 20,
        borderRadius: showMenu ? 15 : 0,
        // transforming view
        transform: [
          { scale: scaleValue },
          { translateX: offsetValue }
        ]
      }}>
        {/* Menu Button */}
        
        <Animated.View style={{
          transform: [{
            translateY: closeButtonOffset
          }]
        }}>
          <TouchableOpacity onPress={() => {
            // Do Actions here
            // Scaling the view
            Animated.timing(scaleValue, {
              toValue: showMenu ? 1 : 0.88,
              duration: 300,
              useNativeDriver: true
            })
              .start()

            Animated.timing(offsetValue, {
              // Your random Value
              toValue: showMenu ? 0 : 230,
              duration: 300,
              useNativeDriver: true
            })
              .start()

            Animated.timing(closeButtonOffset, {
              // Your random value
              toValue: !showMenu ? -30 : 0,
              duration: 300,
              useNativeDriver: true
            })
              .start()

            setShowMenu(!showMenu)
          }}>
            <Image source={showMenu? close : menu} style={{
              width: 20,
              height: 20,
              tintColor: 'black',
              marginTop: 40,
            }}></Image>
          </TouchableOpacity>

          <Text style={{
            fontSize: 30,
            fontWeight: 'bold',
            color: 'black',
            paddingTop: 20
          }}> {currentTab} </Text>

          <Image source={photo} style={{
            width: '100%',
            height: 300,
            borderRadius: 15,
            marginTop: 20
          }}></Image>

          <Text style={{
            fontSize: 20,
            fontWeight: 'bold',
            paddingTop: 15,
            paddingBottom: 5
          }}> Kode </Text>

          <Text>Techie, Coder, Engineer</Text>
        </Animated.View>

      </Animated.View>
    
    </SafeAreaView>
  );
}

const TabButton = ( currentTab, setCurrentTab, title, image, scaleValue, offsetValue, closeButtonOffset, showMenu, setShowMenu ) => {
  return(
    <TouchableOpacity onPress={() => {
      if(title == 'Logout'){
        // Do something
      }else
        setCurrentTab(title);
        
        Animated.timing(scaleValue, {
          toValue: showMenu ? 1 : 0.88,
          duration: 300,
          useNativeDriver: true
        })
          .start()
        
        Animated.timing(offsetValue, {
          // Your random Value
          toValue: showMenu ? 0 : 230,
          duration: 300,
          useNativeDriver: true
        })
          .start()
        
        Animated.timing(closeButtonOffset, {
          // Your random value
          toValue: !showMenu ? -30 : 0,
          duration: 300,
          useNativeDriver: true
        })
          .start()

        setShowMenu(!showMenu)
    }}>
      <View style={{
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 8,
        backgroundColor: currentTab == title ? 'white' : 'transparent',
        paddingLeft: 13,
        paddingRight: 35,
        borderRadius: 8,
        marginTop: 15
      }}>
        <Image source={image} style={{
          width: 25, height: 25,
          tintColor: currentTab == title ? "#5359D1" : "white"
        }}>
        </Image>
        <Text style={{
          fontSize: 15,
          fontWeight: 'bold',
          paddingLeft: 15,
          color: currentTab == title ? "#5359D1" : "white"
        }}
        > { title } </Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    // paddingTop: Platform.OS === 'android' ? 25 : 0,
    flex: 1,
    backgroundColor: '#5359D1',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
});
