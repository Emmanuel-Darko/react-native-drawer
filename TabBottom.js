import React from 'react';
import { View, TouchableOpacity, Image, Text} from 'react-native';
import home from './assets/home.png';
import search from './assets/search.png';
import notifications from './assets/bell.png';
import settings from './assets/settings.png';

export default ({currentTab, setCurrentTab, title, image}) => {
    return(
        <TouchableOpacity>
          <View style={{
            flexDirection: "row",
            alignItems: "center",
            paddingVertical: 8,
            backgroundColor: 'white',
            paddingLeft: 20,
            paddingRight: 40,
            borderRadius: 8
          }}>
            <Image source={image} style={{
              width: 25, height: 25
            }}>
            </Image>
            <Text style={{
              fontSize: 15,
              fontWeight: 'bold',
              paddingLeft: 15
            }}
            > { title } </Text>
          </View>
        </TouchableOpacity>
    )
}
